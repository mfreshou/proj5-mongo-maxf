"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""

# control point can't be over 20% of the race distance
# if the distance is 0 set the closing time to be 1 hour
# if we get a bad inpute the default time is january 1st 2017 at midnight
# 200 km race can only last 13H30


import arrow
import logging
#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
def time_calcs(cont_km, brev_km, speed_list, final_point, closing):
    hours = 0
    mins = 0
    out_times = {'H': 0, 'M': 0}
    tempH = 0
    fin_times = [13, 20, 27, 40, 75]
    rlens = [200.0,300.0,400.0,600.0,1000.0]
    if final_point:
        cont_km = brev_km
        logging.debug("True")
        if closing:
            logging.debug("True")
            for i in range(len(rlens)):
                if brev_km == rlens[i]:
                    if i == 0:
                        mins = 30
                    hours = fin_times[i]
            out_times['H'] = hours
            out_times['M'] = mins
            return out_times


    # this is if the race is less than 600
    # the intervals for the first 3 are all 200 km
    # after 600 km it increases
    for i in range(0,3):
        logging.debug("brev point: {}".format(cont_km))
        logging.debug("hours: {}".format(hours))
        logging.debug("min: {}".format(mins))
        if int(cont_km) <= 200:
            tempH = cont_km//speed_list[i]
            hours += tempH
            mins += (((cont_km/speed_list[i])-tempH)*60)
            cont_km -= cont_km
        else:
            tempH = 200//speed_list[i] # floor division for hours
            cont_km -= 200
            hours += tempH
            mins += (((200/speed_list[i])-tempH)*60)
    if(cont_km > 0):
        tempH = cont_km//speed_list[3]
        hours += tempH
        mins += (((cont_km/speed_list[3])-tempH)*60)
    out_times['H'] = int(hours)
    out_times['M'] = round(mins)
    return out_times


def correct_variables(cont_km, brev_km, stime):
    """
    Args:
        cont_km: control distance in kilometers
        brev_km: distance of the race in kilometers
        stime: ISO 8601 formate date-time string
    Returns: True if all variables are correct type
    """
    goodVars = True
    if not isinstance(cont_km, float) or (cont_km < 0.0):
        logging.info("Please input an integer for distance")
        goodVars = False
    if not isinstance(brev_km, float):
        logging.info("Please input an integer for race distance")
        goodVars = False
    if not isinstance(stime, arrow.arrow.Arrow):
        logging.info("Please input a valid start time")
        goodVars = False
    return goodVars

def race_distance(brev_km):
    """
    Args:
        brev_km: distance of race in kilometers
        must be 200, 300, 400, 600, or 1000 km
    Returns: True if race distance is valid
    """
    rlens = [200,300,400,600,1000]
    retbool = False
    if brev_km in rlens:
        retbool = True
    return retbool

def dis_zero(cont_km):
    """
    Args:
        cont_km: control distance in kilometers
    Returns: True if the control distance is 0
    """
    if int(cont_km) == 0:
        return True
    else:
        return False

def cont_distance(cont_km, brev_km):
    """
    Args:
        cont_km: control distance in kilometers (float)
        brev_km: race distance in kilometers (float)
    Returns: False if the control point is > 20% further then the race distance
    """
    if (cont_km > (brev_km * 1.2)):
        logging.info("Control point distance is more than 20'%' than the length of the race")
        return False
    else:
        return True

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # control is float
    # brevet is int
    speed_listO = [34.0, 32.0, 30.0, 28.0]
    ret_time = 0
    #if control_dist_km < 0.0:
        #logging.info("Invalid inputs, returning default time")
        #return ret_time
    if not correct_variables(control_dist_km, brevet_dist_km, brevet_start_time):
        logging.debug("Invalid inputs, returning default time")
        return ret_time
    elif not race_distance(brevet_dist_km):
        logging.debug("Invalid race distance, returning default time")
        return ret_time
    elif dis_zero(control_dist_km):
        ret_time = brevet_start_time
        logging.debug("Control point at race start")
    elif not cont_distance(control_dist_km, brevet_dist_km):
        logging.debug("Invalid controle point")
        return ret_time
    else:
        if control_dist_km >= brevet_dist_km:
            finalP = True
        else:
            finalP = False
        logging.debug(brevet_start_time)
        otimes = time_calcs(control_dist_km, brevet_dist_km, speed_listO, finalP, False)
        oHours = otimes['H']
        oMins = otimes['M']
        ret_time = brevet_start_time.shift(hours=+oHours, minutes=+oMins)
        logging.debug(brevet_start_time)
        logging.debug(ret_time.isoformat())
    return ret_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    speed_listC = [15.0, 15.0, 15.0, 11.428]
    ret_time = 0
    #if control_dist_km < 0.0:
        #logging.info("Invalid inputs, returning default time")
        #return ret_time
    if not correct_variables(control_dist_km, brevet_dist_km, brevet_start_time):
        logging.debug("Invalid inputs, returning default time")
        return ret_time
    elif not race_distance(brevet_dist_km):
        logging.debug("Invalid race distance, returning default time")
        return ret_time
    elif dis_zero(control_dist_km):
        ret_time = brevet_start_time.shift(hours=+1)
        logging.debug("Control point at race start")
    elif not cont_distance(control_dist_km, brevet_dist_km):
        logging.debug("Invalid controle point")
        return ret_time
    else:
        if control_dist_km >= brevet_dist_km:
            finalP = True
        else:
            finalP = False
        ctimes = time_calcs(control_dist_km, brevet_dist_km, speed_listC, finalP, True)
        ret_time = brevet_start_time.shift(hours=+ctimes['H'], minutes=+ctimes['M'])
        logging.debug(brevet_start_time)
        logging.debug(ret_time.isoformat())
    return ret_time.isoformat()

"""
Check race distance
if control point is greater than race distance then still use that limit
else dont use it
"""
