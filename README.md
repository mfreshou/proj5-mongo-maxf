# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

This version is by Max Freshour

## What is in this repository

You have a minimal implementation of Docker compose in DockerMongo folder, using which you can connect the flask app to MongoDB (as demonstrated in class). Refer to the lecture slide `05a-Table-driven.pdf` and `05b-Docker-Compose.pdf`. You'll also need MongoCommands.txt. Solved acp_times.py file is in Canvas!

## Functionality you'll add

You will reuse *your* code from project 4 (https://bitbucket.org/UOCIS322/proj4-brevets/). Recall: you created a list of open and close controle times using AJAX. In this project, you will create the following functionalities.

1. Create two buttons "Submit" and "Display" in the page where have controle times.
2. On clicking the Submit button, the control times should be entered into the database.
3. On clicking the Display button, the entries from the database should be displayed in a new page.

Handle error cases appropriately. For example, Submit should return an error if there are no controle times. One can imagine many such cases: you'll come up with as many cases as possible.

## Update

Assignment is currently not working properly. Had issues with MongoDB where I did not have enough space.
Issue was docker took up 167 GB of space in volumes and took a while to find out.
Currently able to collect the times and store in database. Display button is not working properly.
The database can be read from in python, but the values were all set as 'km': [array of times] for the distance,
open, and closing times.
Currently is able to build, enter brevet distances, and submit to database. No test cases

## Sources

https://stackoverflow.com/questions/53513/how-do-i-check-if-a-list-is-empty
https://www.w3schools.com/python/python_try_except.asp
https://docs.docker.com/engine/reference/commandline/volume_prune/
https://www.w3schools.com/tags/tag_button.asp
